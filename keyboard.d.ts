declare namespace keyboard {
  const toolKeybinds: Object;

  function isPressed(key: string);
}