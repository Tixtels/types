declare namespace chat {
  /**
   * @param append Note: HTML is not escaped.
   */
  function append(data: string);

  /**
   * @param message Plaintext string to be sent to the server.
   */
  function send(message: string);
}