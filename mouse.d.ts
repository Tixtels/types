declare namespace mouse {
  /**
   * X and Y coordinates of the tixtel the mouse is located at
   */
  let tixtelCoords: number[];
}