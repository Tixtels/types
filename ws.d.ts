declare namespace ws {
  /**
   * @param address A ws:// or wss:// address to connect to
   */
  function connect(addres: string);
  
  /**
   * Note: It is not recommended to manually send data as the protocol can change at any time.
   */
  function send(data: any);
  function reconnect();
  function disconnect();
  function on(event: string, listener: Function);
}