declare namespace options {
  let showGrid: boolean;
  let cameraSpeed: number;
  let blockSize: number;
  let allowTextPaste: boolean;
  let rank: number;
}