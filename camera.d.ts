declare type CameraDirection = "up" | "left" | "down" | "right";

declare namespace camera {
  /**
   * @param direction up | left | down | right
   */
  function move(direction: CameraDirection);
  
  function teleport(x: number, y: number);
  
  function toggleGrid();
  
  function getZoom(): number;
  function setZoom(value: number);
  
  function zoomOut(strength: number); // 2
  function zoomIn(strength: number); // 2
}