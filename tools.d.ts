declare namespace tools {
  interface ToolOptions {
    icon?: string;
    keybind?: string;
  }
  
  class Tool {
    constructor(name: string, opts: ToolOptions);
  }

  export { Tool as tool };

  /**
   * @param tool A class that extends Tools.tool
   */
  export function add(tool: any) : Tool;

  /**
   * @param tool A class that extends Tools.tool
   */
  export function remove(tool: any);
  
  /**
   * @param tool A class that extends Tools.tool
   */
  export function select(tool: any);
}