class ToolOption {
  constructor(tool: tools.tool);
  
  open() : ToolOption;
  
  addSlider(min: number, max: number, oninput: Function) : ToolOption;
  
  addLabel(text?: string) : ToolOption;
  enterLabel() : ToolOption;
  exitLabel() : ToolOption;
  
  appendText(text: string) : ToolOption;
  
  addCheckbox(checked: boolean, onchange: Function) : ToolOption;
  
  addSelect(options: any, onchange: Function) : ToolOption;
  
  addInput(type: string, placeholder: string, oninput: Function) : ToolOption;
  
  addButton(text: string, onclick: Function) : ToolOption;
  
  getLast() : HTMLElement;
}