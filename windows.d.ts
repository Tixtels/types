declare namespace windows {
  interface WindowOptions {
    closeable?: boolean;
    hideOnClose?: boolean;
    centered?: boolean;
    resizeable?: boolean;
    movable?: boolean;
  }
  class Window {
    title: string;
    coords: number[]; // get|set
    shown: boolean;

    constructor(title: string, options: WindowOptions);
    move(x: number, y: number);
    
    /**
     * Updates the window coordinates to the current position on the browser window
     */
    updateCoords();
    
    header: HTMLDivElement; // get
    container: HTMLDivElement; // get
    content: string; // get|set
    
    hide();
    show();
    close();
    center();
  }

  export { Window as window };

  /**
   * @param win A class that extends Windows.window
   */
  export function add(win: any) : Window;
  
  /**
   * @param win A class that extends Windows.window
   */
  export function remove(win: any) : Window;
}