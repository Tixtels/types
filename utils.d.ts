declare namespace utils {
  function getRawXY(x: number, y: number);
  function getXY(chunkX: number, chunkY: number, blockX: number, blockY: number);
  
  function setTheme(css: string);
  function byId(id: string) : HTMLElement;
  function html(tag: string, properties: Object) : HTMLElement;
  function htmlEscape(html: string) : string;
  
  function getTixtel(x: number, y: number);
  function setColor(x: number, y: number, rgb: number[]);
  function setChar(x: number, y: number, char: string);
  
  function line(startX: number, startY: number, endX: number, endY: number, color: number[]);
  function rectangle(startX: number, startY: number, endX: number, endY: number, color: number[]);
}