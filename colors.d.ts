declare namespace colors {
  color: number[];
  // colors
  
  // save
  // load
  
  function choose(rgb: number[]);
  function add(rgb: number[]);
  function bulkAdd(colors: number[][]);
  function remove(rgb: number[]);
  function removeAll();
}